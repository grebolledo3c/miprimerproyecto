package cl.grebolledoa.miprimeraaplicacionandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText myNumber1;
    private EditText myNumber2;
    private TextView myResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setView();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("resultado", myResult.getText().toString());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        myResult.setText(savedInstanceState.getString("resultado"));
    }

    public void setView(){
        this.myNumber1 = (EditText) findViewById(R.id.myNumber1);
        this.myNumber2 = (EditText) findViewById(R.id.myNumber2);
        this.myResult = findViewById(R.id.txtResultado);
    }

    public void myButtonClick(View view){
        calcular();
    }

    private void calcular(){
        if(!isNullOrEmpty(myNumber1.getText().toString()) && !isNullOrEmpty(myNumber2.getText().toString())  )
        {
            int valor1 = Integer.parseInt(myNumber1.getText().toString());
            int valor2 = Integer.parseInt(myNumber2.getText().toString());
            int resultado = valor1 + valor2;
            myResult.setText("El resultado de la suma entre [" + valor1 + " y " + valor2 + "] es de " + resultado);
        }
    }

    private boolean isNullOrEmpty(String cadena){
        if(cadena == null || cadena.isEmpty()){
            return true;
        }
        return false;
    }
}